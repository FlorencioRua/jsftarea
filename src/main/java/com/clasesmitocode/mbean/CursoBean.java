package com.clasesmitocode.mbean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.clasesmitocode.dao.CursoDao;
import com.clasesmitocode.model.Curso;
import com.clasesmitocode.service.CursoService;


@Named
@ViewScoped
public class CursoBean implements Serializable {
	
	private List<Curso> lista;
	private Curso curso;
	private CursoService service;
	

	public CursoBean() {
		lista = new ArrayList<>();
		service = new CursoService(new CursoDao());		
		this.listar();				
	}
	

	public void listar() {
		this.lista = service.listar();
	}

	public void enviar(Curso curso) {
		this.curso = curso;
	}


	public List<Curso> getLista() {
		return lista;
	}


	public void setLista(List<Curso> lista) {
		this.lista = lista;
	}


	public Curso getCurso() {
		return curso;
	}


	public void setCurso(Curso curso) {
		this.curso = curso;
	}
	
	
	

}
