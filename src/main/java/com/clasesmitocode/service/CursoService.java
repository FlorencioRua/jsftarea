package com.clasesmitocode.service;

import java.io.Serializable;
import java.util.List;

import com.clasesmitocode.dao.IDao;
import com.clasesmitocode.model.Curso;

public class CursoService implements Serializable{

	private IDao dao;
	
	public CursoService(IDao idao) {
		dao=idao;
	}
	
	
	public List<Curso> listar(){
		return dao.listar();
	}
	
	
	
}
