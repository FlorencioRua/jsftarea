package com.clasesmitocode.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.clasesmitocode.model.Curso;

public class CursoDao implements Serializable,IDao{

	@Override
	public List<Curso> listar() {
			List<Curso> lista = new ArrayList<>();
			for (int i = 0; i < 100; i++) {
				Curso curso = new Curso();
				curso.setIdCurso(i); 
				curso.setNombre("Programación orientada a objetos");
				curso.setHoras(6);
				lista.add(curso);
			}
			
			return lista;
		}
	

}
