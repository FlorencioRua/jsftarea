package com.clasesmitocode.dao;

import java.util.List;

import com.clasesmitocode.model.Curso;


public interface IDao {
	
	List<Curso> listar();

}
